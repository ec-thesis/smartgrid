#!/bin/bash
# Build + push all application logic docker images, s.t. they can be deployed to k8s using smartgrid CRDs

set -eu

docker build --build-arg BUILDAPP=agent-app -t localhost:5001/smartgridagent-app -t localhost:5001/smartgridagent-app:latest .
docker push localhost:5001/smartgridagent-app:latest

docker build --build-arg BUILDAPP=community-app -t localhost:5001/smartgridcommunity-app -t localhost:5001/smartgridcommunity-app:latest .
docker push localhost:5001/smartgridcommunity-app:latest

docker build --build-arg BUILDAPP=ems-app -t localhost:5001/smartgridems-app -t localhost:5001/smartgridems-app:latest .
docker push localhost:5001/smartgridems-app:latest

echo "Build successfull"
#echo "  docker run -p 8080:8080 -it -e AGENT_CONFIG='{\"type\": \"simulation\"}' localhost:5001/smartgridagent-app:latest"
#echo "  docker run -p 8080:8080 -it -e COMMUNITY_CONFIG='{\"type\": \"simulation\"}' localhost:5001/smartgridcommunity-app:latest"
#echo "  docker run -p 8080:8080 -it -e COMMUNITY=ec1 -e EMS_STRATEGY=sumzero -e EMS_CONFIG='{\"type\": \"simulation\"}' localhost:5001/smartgridems-app:latest"