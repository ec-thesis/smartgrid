#!/bin/bash
# Build, deploy, run all scenarios and evaluations

set -eu

test -f ./app.py || (echo "wrong folder"; exit 1)

# $1: Scenario
function run() {
	echo
	echo "*********** Starting $1..."
	pushd ../../scenario
	./run-scenario.sh $1
	popd

	echo
	echo "*********** Deployed $1, running simulation in 10 sec..."
	sleep 10
	RUNNER_CONFIG='{"ems_name": "'$1'"}' python3 app.py

	echo
	echo "*********** Completed $1"
	echo
}


run sumzero2esr2cons
run noop2esr2cons
run individual2esr2cons

run sumzero4esr4cons
run noop4esr4cons
run individual4esr4cons

run sumzero8esr8cons
run noop8esr8cons
run individual8esr8cons

run sumzero6esr18cons
run sumzero18esr6cons
