#!/bin/bash
# Build, deploy, run all scenarios and evaluations

set -eu

test -f ./app.py || (echo "wrong folder"; exit 1)

# $1: Scenario
function run() {
	echo
	echo "*********** Starting $1..."
	pushd ../../scenario
	./run-scenario.sh $1
	popd

	echo
	echo "*********** Deployed $1, running simulation in 60 sec..."
	sleep 60
	RUNNER_CONFIG='{"ems_name": "'$1'"}' python3 app.py

	echo
	echo "*********** Completed $1"
	echo
}


### CREATE CSV ###
csvfile=./results.csv
echo "scenario;health_individual;health_noop;health_sumzero;autarky_individual;autarky_noop;autarky_sumzero;equilibrium_individual;equilibrium_noop;equilibrium_sumzero;reward_individual;reward_noop;reward_sumzero;" > $csvfile

# $1 scenario
# $2 strategy
# $3 json value
function get_totals_value() {
	cat output_${2}${1}/week_totals.json 2>/dev/null | jq .${3} | tr '.' ',' || echo ""
}

# Exact order like in excel
for scn in 2esr2cons 4esr4cons 2esr6cons 3esr9cons 0esr24cons 2esr22cons 4esr20cons 6esr18cons 9esr15cons 12esr12cons 18esr6cons 24esr0cons 
do
	echo -n $scn";" >> $csvfile
    echo -n $(get_totals_value $scn individual community_onlinerate)";" >> $csvfile
    echo -n $(get_totals_value $scn noop       community_onlinerate)";" >> $csvfile
    echo -n $(get_totals_value $scn sumzero    community_onlinerate)";" >> $csvfile
    echo -n $(get_totals_value $scn individual community_autarky)";" >> $csvfile
    echo -n $(get_totals_value $scn noop       community_autarky)";" >> $csvfile
    echo -n $(get_totals_value $scn sumzero    community_autarky)";" >> $csvfile
    echo -n $(get_totals_value $scn individual community_equilib)";" >> $csvfile
    echo -n $(get_totals_value $scn noop       community_equilib)";" >> $csvfile
    echo -n $(get_totals_value $scn sumzero    community_equilib)";" >> $csvfile
    echo -n $(get_totals_value $scn individual balance_eur)";" >> $csvfile
    echo -n $(get_totals_value $scn noop       balance_eur)";" >> $csvfile
    echo -n $(get_totals_value $scn sumzero    balance_eur)";" >> $csvfile
    echo >> $csvfile
done


echo "--------"
cat $csvfile

