""" SmartGridAgent (stateless) """

__author__ = "Stephan Podlipnig"

from flask import Flask, request
from datetime import datetime, timedelta
import time
import httpx
import asyncio
import json
import os
import random

import numpy
import matplotlib.pyplot as plt
import matplotlib.dates as md

def load_config(default_config):
    try:
        param_config = json.loads(os.environ.get('RUNNER_CONFIG', "{}"))
        return {**default_config, **param_config}
    except:
        raise ValueError("Invalid or missing config json")

config = load_config({
    'start': datetime(2022,8,29,0,0,0),
    #'start': datetime(2022,9,1,0,0,0),
    'end': datetime(2022,9,5,0,0,0),
    #'end': datetime(2022,8,30,12,0,0),
    'ems_name': 'sumzero2esr2cons',
    'strategy_name': None,
    'output_name': None,

    'timeout_sec': 15,
    'ems_url_schema': 'http://localhost/{}/ems',
    'output_folder': "output"
})

app = Flask(__name__)

def request_async(client, url, method='GET', json=None):
    return client.request(method, url, timeout=config['timeout_sec'], json=json)

async def query_ems(timeslot=""):
    async with httpx.AsyncClient() as client:
        url = ''.join([config['ems_url_schema'].format(config['ems_name']), "?timeslot=", timeslot])
        r = await asyncio.ensure_future(request_async(client, url))
        print(''.join(["Completed: ", timeslot]))
        return json.loads(r.content)['data']

resultdata = {}
members = []
totals = []
dates = []

def pushStep(ts):
    resultdata[ts] = dict(asyncio.run(query_ems(ts)))

def getMemberEnergyFlowSeries(member, key, scalefactor=1):
    series = {}
    for k, v in resultdata.items():
        if v['details']['details'].get(member):
            series[k] = v['details']['details'][member]['energyflow'][key] * scalefactor
        else:
            series[k] = numpy.nan
    return series

def plotMemberGraph(member, fileprefix=""):
    plt.interactive(True)
    fig, ax1 = plt.subplots()
    fig.set_size_inches(20, 10)
    plt.gca().xaxis.set_minor_locator(md.HourLocator(interval=6))  # every x hours
    plt.gca().xaxis.set_minor_formatter(md.DateFormatter('%H:%M'))  # hours and minutes
    plt.gca().xaxis.set_major_locator(md.DayLocator(interval=1))  # every day
    plt.gca().xaxis.set_major_formatter(md.DateFormatter('\n%Y-%m-%d'))
    plt.grid(color='grey', linestyle=':', linewidth=0.5, which='both')

    # Y-axis left
    ax1.set_ylabel("Power [kW]")
    ax1.plot_date(dates, getMemberEnergyFlowSeries(member, "battery_power_rate", -0.001).values(), 'r', label='ESR (charging) [kW]')
    ax1.plot_date(dates, getMemberEnergyFlowSeries(member, "consumption_power_rate", 0.001).values(), 'steelblue', label='Consumption [kW]')
    ax1.stackplot(dates, getMemberEnergyFlowSeries(member, "production_power_rate", 0.001).values(), colors=['khaki'], labels=['Production (PV) [kW]'])
    ax1.plot_date(dates, getMemberEnergyFlowSeries(member, "grid_import_ems_target", 0.001).values(), 'k+', label='EMS power target (add. import) [kW]')
    ax1.plot_date(dates, getMemberEnergyFlowSeries(member, "grid_power_rate", 0.001).values(), 'k:', label='Grid (importing) [kW]')
    #ax1.plot_date(dates, getMemberEnergyFlowSeries(member, "battery_energy_available", 0.001).values(), 'purple', label='battery_energy_available [kWh]')
    #ax1.plot_date(dates, getMemberEnergyFlowSeries(member, "battery_energy_change_relative", 0.001).values(), 'purple', label='battery_energy_change_relative [kWh]')
    #ax1.plot_date(dates, getCommunitySeries("total_grid_power_rate", 0.001).values(), 'darkgray', label='total_grid_power_rate (total) [kW]')
    low, high = plt.ylim()
    bound = max(abs(low), abs(high))
    plt.ylim(-bound, bound)
    ax1.margins(0, 0)
    ax1.legend(loc="lower left", facecolor='white', framealpha=0)

    # Y-axis right
    ax2 = ax1.twinx()
    ax2.set_ylabel('%')
    ax2.set_ylim(ymin=0, ymax=100)
    ax2.plot_date(dates, getMemberEnergyFlowSeries(member, "battery_soc", 100).values(), 'r--,', label='ESR SoC [%]')
    ax2.plot_date(dates, getMemberEnergyFlowSeries(member, "acc_selfconsumption_ratio", 100).values(), 'y--', label='Self consumption (autarky) [%]')
    ax2.plot_date(dates, getEmsTotalsSeries("community_independency", 100).values(), 'g--', label='Community consumption (autarky) [%]')
    #ax2.plot_date(dates, getEmsTotalsSeries("community_equilib_momentary", 100).values(), 'b--', label='Community equilibrium [%]')
    ax2.legend(loc="lower right", facecolor='white', framealpha=0)

    # Highlight missing values
    for nan in getMissingValueList(member):
        plt.axvspan(nan - numpy.timedelta64(7, 'm'), nan + numpy.timedelta64(8, 'm'), facecolor='r', alpha=0.25)
        plt.axvspan(nan - numpy.timedelta64(20, 's'), nan + numpy.timedelta64(20, 's'), facecolor='r')

    plt.ion()
    plt.savefig(config['output_folder']+'_'+config['output_name'] + '/' + fileprefix + member + '.png', dpi=300, bbox_inches='tight')
    plt.close(fig)


def getEmsMemberDetailsSeries(member, key, scalefactor=1):
    series = {}
    for k, v in resultdata.items():
        series[k] = v['tracking']['details'][member]['cumulativeValues'][key] * scalefactor
    return series

def getCommunitySeries(key, scalefactor=1):
    series = {}
    for k, v in resultdata.items():
        series[k] = v['details']['energyflow'][key] * scalefactor
    return series

def getEmsTotalsSeries(key, scalefactor=1):
    series = {}
    for k, v in resultdata.items():
        series[k] = v['tracking']['totals'][key] * scalefactor
    return series

def getMissingValueList(member):
    list = []
    for k, v in resultdata.items():
        if not v['details']['details'].get(member) or not v['details']['details'][member]['is_available']:
            list.append(numpy.datetime64(k))
    return list

def plotEmsGraph(fileprefix=""):
    plt.interactive(True)
    fig, ax1 = plt.subplots()
    fig.set_size_inches(20, 10)
    plt.gca().xaxis.set_minor_locator(md.HourLocator(interval=6))  # every x hours
    plt.gca().xaxis.set_minor_formatter(md.DateFormatter('%H:%M'))  # hours and minutes
    plt.gca().xaxis.set_major_locator(md.DayLocator(interval=1))  # every day
    plt.gca().xaxis.set_major_formatter(md.DateFormatter('\n%Y-%m-%d'))
    plt.grid(color='grey', linestyle=':', linewidth=0.5, which='both')

    # Y-axis left
    ax1.set_ylabel("Power [kW]")

    ax1.plot_date(dates, getCommunitySeries("total_grid_power_export_rate", 0.001).values(), 'darkgreen', linewidth=0.5, label='Sum exports by all members')
    ax1.plot_date(dates, getCommunitySeries("total_grid_power_import_rate", -0.001).values(), 'r', linewidth=0.5, label='Sum imports by all members')

    ax1.stackplot(dates,
                  getCommunitySeries("total_grid_power_export_rate", 0.001).values(),
                  colors=['green'],
                  labels=[r'Excess: Members => Central Grid. Sold %.0f kWh @ %.0f cent/kWh' % (totals['gridexported_energy']/1000, totals['price_grid_export']*1000*100, )]
                  )

    ax1.stackplot(dates,
                  getCommunitySeries("total_grid_power_import_rate", -0.001).values(),
                  colors=['r'],
                  labels=[ r'Demand: Central grid => Members. Bought %.0f kWh @ %.0f cent/kWh' % (totals['gridimported_energy']/1000, totals['price_grid_import']*1000*100, )]
                  )

    ax1.stackplot(dates,
                  getEmsTotalsSeries("community_power_import_rate_among_members", 0.001).values(),
                  colors=['silver'],
                  labels=[ '']
                  )

    ax1.stackplot(dates,
                  getEmsTotalsSeries("community_power_export_rate_among_members", -0.001).values(),
                  colors=['lightgrey'],
                  labels=[ r'Redistribution: Members => Members. %.0f kWh @ (%.0f cent/kWh export - %.0f cent/kWh import)' % (totals['communityimported_energy']/1000, totals['price_community_export']*1000*100, totals['price_community_import']*1000*100, )]
                  )

    #for member in members:
    #    ax1.plot_date(dates, getEmsMemberDetailsSeries(member, "grid_cost").values(), label=member+' grid_cost [EUR]')

    low, high = plt.ylim()
    bound = max(abs(low), abs(high))
    plt.ylim(-bound, bound)
    ax1.margins(0, 0)
    ax1.legend(loc="lower left", facecolor='white', framealpha=0)

    # Y-axis right
    ax2 = ax1.twinx()
    ax2.set_ylabel('EUR')
    p1 = ax2.plot_date(dates, getEmsTotalsSeries("balance_eur", 1).values(), 'steelblue', label='Total reward (sum over all members) [EUR]')
    #ax2.set_ylabel('%')
    #ax2.set_ylim(ymin=0, ymax=100)
    #ax2.plot_date(dates, getEmsTotalsSeries("community_independency", 100).values(), 'g--', label='community_independency [%]')
    #ax2.plot_date(dates, getEmsTotalsSeries("community_equilib", 100).values(), 'y--', label='community_equilib [%]')
    #ax2.legend(loc="lower right", facecolor='white', framealpha=0)

    # Y-axis 3
    ax3 = ax1.twinx()
    ax3.spines.right.set_position(("axes", 1.045))
    ax3.set_ylabel('%')
    ax3.set_ylim(ymin=0, ymax=100)
    p2 = ax3.plot_date(dates, getEmsTotalsSeries("community_autarky", 100).values(), 'g--', linewidth=1, label='Autarky (community self-consumption) [%]')
    p3 = ax3.plot_date(dates, getEmsTotalsSeries("community_battery_soc_momentary", 100).values(), 'r--', linewidth=1, label='SoC (avg. over all ESRs) [%]')
    # ax3.plot_date(dates, getEmsTotalsSeries("community_equilib_momentary", 100).values(), 'b--', linewidth=1, label='Community equilibrium [%]')
    ax3.legend(p1+p2+p3, [l.get_label() for l in p1+p2+p3], loc="lower right", facecolor='white', framealpha=0)

    # Textbox
    textstr = '\n'.join((
        r'Availability:  %.2f %% ' % (totals['community_onlinerate'] * 100,),
        r'Autarky:  %.2f %% ' % (totals['community_autarky']*100,),
        r'Equilibrium:  %.2f %% ' % (totals['community_equilib']*100,),
        r'Avg.SoC.:   %.2f %% ' % (totals['community_battery_soc']*100,),
        r'Total ESR cap.:  %.0f kWh ' % (totals['community_esr_capacity']/1000,),
        r'Reward:    %.2f € ' % (totals['balance_eur'],)
    ))
    ax2.text(0.98, 0.85, textstr, transform=ax2.transAxes, ha='right', fontsize=12, bbox=dict(boxstyle="round", ec=('w'), fc=('w'),) )

    # Title
    #plt.title('EC: ' + config['ems_name'].replace("sumzero", "").replace("invidividual", "").replace("noop", "").replace("esr", "esr ") + "   -   Strategy: "+config['strategy_name'])

    plt.ion()
    plt.savefig(config['output_folder']+'_'+config['output_name']+'/'+ fileprefix + 'ems.png', dpi=300, bbox_inches='tight')
    plt.close(fig)

def run_ems_simulaton():
    # fast-forward simulate ems once over whole week
    for i in numpy.arange(config['start'], config['end'], timedelta(minutes=15)).astype(datetime):
        time.sleep(0.001) # wait 1ms per 15min step
        pushStep(str(i).replace(" ", "T"))

    if not os.path.exists(config['output_folder']+'_'+config['output_name']):
        os.makedirs(config['output_folder']+'_'+config['output_name'])

    # write cache
    with open(config['output_folder']+'_'+config['output_name'] + '/resultlist.json', 'w') as f:
        f.write(json.dumps(resultdata))


def run_cached_simulation():
    # load cache
    with open(config['output_folder']+'_'+config['output_name'] + '/resultlist.json', 'r') as f:
        for k, v in json.load(f).items():
            resultdata[k] = v

def generate_fault_schedule():
    schedule = {}
    for h in range(1, 24+1):
        data = []
        behavior = ""
        behavior_ctr = 1
        for i in numpy.arange(config['start'], config['end'], timedelta(minutes=15)).astype(datetime):
            if behavior_ctr < 8: # block length. set 1 to roll dice every 15min, 8 for every 2 hours
                behavior_ctr += 1
            else:
                behavior = "online" if random.random() < 0.25 else "timeout"
                behavior_ctr = 1
            
            data.append({'time': str(i), 'behavior': behavior})

        schedule[r'home%02d' % (h,)] = data

    with open('fault_schedule.json', 'w') as f:
        f.write(json.dumps(schedule))

if __name__ == "__main__":

    if not config['output_name']:
        config['output_name'] = config['ems_name']
    if not config['strategy_name']:
        config['strategy_name'] = config['output_name']

    #generate_fault_schedule()
    #exit()

    run_ems_simulaton()
    #run_cached_simulation()

    dates = [numpy.datetime64(ts) for ts in numpy.arange(config['start'], config['end'], timedelta(minutes=15)).astype(datetime)]
    members = resultdata[str(config['start']).replace(" ", "T")]['details']['all_members']

    with open(config['output_folder'] + '_' + config['output_name'] + '/week_totals.json', 'w') as f:
        for key in resultdata.keys(): pass
        totals = resultdata[key]['tracking']['totals']
        f.write(json.dumps(totals))

    # create individual graphs
    for member in members:
        plotMemberGraph(member, "week_")

    plotEmsGraph("week_")

    # --- strip resultset to 1 day only. must be very first day, otherwise wrong day_totals! ---
    dates = [numpy.datetime64(ts) for ts in numpy.arange(datetime(2022,8,29,0,0,0), datetime(2022,8,30,0,0,0), timedelta(minutes=15)).astype(datetime)]
    for k in list(resultdata.keys()):
        if not "2022-08-29" in k:
            del resultdata[k]

    with open(config['output_folder'] + '_' + config['output_name'] + '/day_totals.json', 'w') as f:
        for key in resultdata.keys(): pass
        totals = resultdata[key]['tracking']['totals']
        f.write(json.dumps(totals))

    # create individual graphs
    for member in members:
        plotMemberGraph(member, "day_")

    plotEmsGraph("day_")

