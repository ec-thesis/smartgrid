#!/bin/bash
# Build, deploy, run all scenarios and evaluations

set -eu

test -f ./app.py || (echo "wrong folder"; exit 1)

# $1: Scenario
function run() {
	echo
	echo "*********** Starting $1..."
	pushd ../../scenario
	./run-scenario.sh $1
	popd

	echo
	echo "*********** Deployed $1, running simulation in 60 sec..."
	sleep 30
	RUNNER_CONFIG='{"ems_name": "'$1'", "strategy_name": "'$2'"}' python3 app.py

	echo
	echo "*********** Completed $1"
	echo
}

run sumzero2esr2cons SumZero
run sumzero4esr4cons SumZero
run sumzero0esr24cons SumZero
run sumzero2esr22cons SumZero
run sumzero4esr20cons SumZero
run sumzero6esr18cons SumZero
run sumzero9esr15cons SumZero
run sumzero12esr12cons SumZero
run sumzero18esr6cons SumZero
run sumzero24esr0cons SumZero

run noop2esr2cons NoOp
run noop4esr4cons NoOp
run noop0esr24cons NoOp
run noop2esr22cons NoOp
run noop4esr20cons NoOp
run noop6esr18cons NoOp
run noop9esr15cons NoOp
run noop12esr12cons NoOp
run noop18esr6cons NoOp
run noop24esr0cons NoOp

run individual2esr2cons Individual
run individual4esr4cons Individual
run individual0esr24cons Individual
run individual2esr22cons Individual
run individual4esr20cons Individual
run individual6esr18cons Individual
run individual9esr15cons Individual
run individual12esr12cons Individual
run individual18esr6cons Individual
run individual24esr0cons Individual

# specials
run sumzero2esr6cons SumZero
run sumzero3esr9cons SumZero


### CREATE CSV ###
csvfile=./results.csv
echo "scenario;health_individual;health_noop;health_sumzero;autarky_individual;autarky_noop;autarky_sumzero;equilibrium_individual;equilibrium_noop;equilibrium_sumzero;reward_individual;reward_noop;reward_sumzero; production;consumption;" > $csvfile

# $1 scenario
# $2 strategy
# $3 json value
function get_totals_value() {
	cat output_${2}${1}/week_totals.json 2>/dev/null | jq .${3} | tr '.' ',' || echo ""
}

# Exact order like in excel
for scn in 2esr2cons 4esr4cons 2esr6cons 3esr9cons 0esr24cons 2esr22cons 4esr20cons 6esr18cons 9esr15cons 12esr12cons 18esr6cons 24esr0cons 
do
	echo -n $scn";" >> $csvfile
	echo -n $(get_totals_value $scn individual community_onlinerate)";" >> $csvfile
	echo -n $(get_totals_value $scn noop       community_onlinerate)";" >> $csvfile
	echo -n $(get_totals_value $scn sumzero    community_onlinerate)";" >> $csvfile
	echo -n $(get_totals_value $scn individual community_autarky)";" >> $csvfile
	echo -n $(get_totals_value $scn noop       community_autarky)";" >> $csvfile
	echo -n $(get_totals_value $scn sumzero    community_autarky)";" >> $csvfile
	echo -n $(get_totals_value $scn individual community_equilib)";" >> $csvfile
	echo -n $(get_totals_value $scn noop       community_equilib)";" >> $csvfile
	echo -n $(get_totals_value $scn sumzero    community_equilib)";" >> $csvfile
	echo -n $(get_totals_value $scn individual balance_eur)";" >> $csvfile
	echo -n $(get_totals_value $scn noop       balance_eur)";" >> $csvfile
	echo -n $(get_totals_value $scn sumzero    balance_eur)";" >> $csvfile

	# strategy-independent
	echo -n $(get_totals_value $scn sumzero    produced_energy)";" >> $csvfile
	echo -n $(get_totals_value $scn sumzero    consumed_energy)";" >> $csvfile
	echo >> $csvfile
done


echo "--------"
cat $csvfile

