#!/bin/bash
# Build, deploy, run all scenarios and evaluations

set -eu

test -f ./app.py || (echo "wrong folder"; exit 1)

# $1: Scenario
function run() {
	echo
	echo "*********** Starting $1_$2..."
	pushd ../../scenario
	./run-scenario.sh $1 $2
	popd

	echo
	echo "*********** Deployed $1_$2, running simulation in 60 sec..."
	sleep 30
	RUNNER_CONFIG='{"ems_name": "'$1'", "output_name": "'$1'_'$2'"}' python3 app.py

	echo
	echo "*********** Completed $1_$2"
	echo
}

run sumzero12esr12cons ""
run sumzero12esr12cons 15min_random_98
run sumzero12esr12cons 15min_random_95
run sumzero12esr12cons 15min_random_90
run sumzero12esr12cons 15min_random_80
run sumzero12esr12cons 15min_random_65
run sumzero12esr12cons 15min_random_50
run sumzero12esr12cons 15min_random_25

run sumzero12esr12cons 120min_random_98
run sumzero12esr12cons 120min_random_95
run sumzero12esr12cons 120min_random_90
run sumzero12esr12cons 120min_random_80
run sumzero12esr12cons 120min_random_65
run sumzero12esr12cons 120min_random_50
run sumzero12esr12cons 120min_random_25

run sumzero4esr20cons ""
run sumzero4esr20cons 15min_random_98
run sumzero4esr20cons 15min_random_95
run sumzero4esr20cons 15min_random_90
run sumzero4esr20cons 15min_random_80
run sumzero4esr20cons 15min_random_65
run sumzero4esr20cons 15min_random_50
run sumzero4esr20cons 15min_random_25

run sumzero4esr20cons 120min_random_98
run sumzero4esr20cons 120min_random_95
run sumzero4esr20cons 120min_random_90
run sumzero4esr20cons 120min_random_80
run sumzero4esr20cons 120min_random_65
run sumzero4esr20cons 120min_random_50
run sumzero4esr20cons 120min_random_25

exit 

### CREATE CSV ###
csvfile=./results_cs2_faultinjection.csv
echo "scenario;15min_community_onlinerate;120min_community_onlinerate;15min_community_autarky;120min_community_autarky;15min_community_equilib;120min_community_equilib;15min_balance_eur;120min_balance_eur;" > $csvfile

# $1 scenario
# $2 strategy
# $3 json value
function get_totals_value() {
	cat output_${1}/week_totals.json 2>/dev/null | jq .${2} | tr '.' ',' || echo ""
}

# Exact order like in excel
for scn in sumzero12esr12cons sumzero4esr20cons
do

	for perc in 25 50 65 80 90 95 98 100
	do
		echo -n $scn";" >> $csvfile
		echo -n $(get_totals_value "${scn}_15min_random_${perc}"  community_onlinerate)";" >> $csvfile
		echo -n $(get_totals_value "${scn}_120min_random_${perc}" community_onlinerate)";" >> $csvfile
		echo -n $(get_totals_value "${scn}_15min_random_${perc}"  community_autarky)";" >> $csvfile
		echo -n $(get_totals_value "${scn}_120min_random_${perc}" community_autarky)";" >> $csvfile
		echo -n $(get_totals_value "${scn}_15min_random_${perc}"  community_equilib)";" >> $csvfile
		echo -n $(get_totals_value "${scn}_120min_random_${perc}" community_equilib)";" >> $csvfile
		echo -n $(get_totals_value "${scn}_15min_random_${perc}"  balance_eur)";" >> $csvfile
		echo -n $(get_totals_value "${scn}_120min_random_${perc}" balance_eur)";" >> $csvfile
		echo >> $csvfile
	done

done

echo "--------"
cat $csvfile

