#!/bin/bash
# Build, deploy, run all scenarios and evaluations

set -eu

test -f ./app.py || (echo "wrong folder"; exit 1)

# $1: Scenario
function run() {
	echo
	echo "*********** Starting $1_$2..."
	pushd ../../scenario
	./run-scenario.sh $1 null $2
	popd

	echo
	echo "*********** Deployed $1_$2, running simulation in 60 sec..."
	sleep 30
	RUNNER_CONFIG='{"ems_name": "'$1'", "output_name": "'$1'_'$2'", "strategy_name": "'$2'"}' python3 app.py

	echo
	echo "*********** Completed $1_$2"
	echo
}

run sumzero12esr12cons ""
exit

run sumzero12esr12cons SumZeroWithConsShift50perc4h
run sumzero12esr12cons SumZeroWithConsShift50perc2h
run sumzero12esr12cons ""

run sumzero9esr15cons SumZeroWithConsShift50perc4h
run sumzero9esr15cons SumZeroWithConsShift50perc2h
run sumzero9esr15cons ""

### CREATE CSV ###
csvfile=./results_cs3_loadshifting.csv
echo "scenario;community_autarky;2h_community_autarky;4h_community_autarky;community_equilib;2h_community_equilib;4h_community_equilib;balance_eur;2h_balance_eur;4h_balance_eur; production;consumption;" > $csvfile

# $1 scenario
# $2 strategy
# $3 json value
function get_totals_value() {
	cat output_${1}_${2}/week_totals.json | jq .${3} | tr '.' ',' || echo ""
}

# Exact order like in excel
for scn in sumzero12esr12cons sumzero9esr15cons
do
	echo -n $scn";" >> $csvfile
	echo -n $(get_totals_value $scn ""                           community_autarky)";" >> $csvfile
	echo -n $(get_totals_value $scn SumZeroWithConsShift50perc2h community_autarky)";" >> $csvfile
	echo -n $(get_totals_value $scn SumZeroWithConsShift50perc4h community_autarky)";" >> $csvfile
	echo -n $(get_totals_value $scn ""                           community_equilib)";" >> $csvfile
	echo -n $(get_totals_value $scn SumZeroWithConsShift50perc2h community_equilib)";" >> $csvfile
	echo -n $(get_totals_value $scn SumZeroWithConsShift50perc4h community_equilib)";" >> $csvfile
	echo -n $(get_totals_value $scn ""                           balance_eur)";" >> $csvfile
	echo -n $(get_totals_value $scn SumZeroWithConsShift50perc2h balance_eur)";" >> $csvfile
	echo -n $(get_totals_value $scn SumZeroWithConsShift50perc4h balance_eur)";" >> $csvfile

	# strategy-independent
	echo -n $(get_totals_value $scn ""    produced_energy)";" >> $csvfile
	echo -n $(get_totals_value $scn ""    consumed_energy)";" >> $csvfile

	echo >> $csvfile
done
echo "--------"
cat $csvfile


