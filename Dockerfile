FROM python:3.9

ARG BUILDAPP agent-app

ENV APPDIR /app
ENV PYTHONUNBUFFERED 1
ENV FLASK_ENV production

WORKDIR $APPDIR

COPY requirements.txt .
RUN python3 -m pip install -r requirements.txt --no-cache-dir

COPY $BUILDAPP .

CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0", "--port=8080"]

EXPOSE 8080
