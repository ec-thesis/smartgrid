from ems.individual import IndividualEMS
from ems.noop import NoopEMS
from ems.sumzero import SumZeroEMS
from exceptions import *


class EmsFactory:
    def __init__(self, stategy, emsconfig):
        try:
            self.stategy = stategy
            self.config = emsconfig
        except:
            raise ValidationError("Invalid or no ems config json")

        try:
            emstypes = {
                "individual": IndividualEMS,
                "noop": NoopEMS,
                "sumzero": SumZeroEMS,
            }
            self.ems = emstypes[self.stategy](self.config)
        except:
            raise ValidationError("Invalid ems strategy or config")

    def getEms(self):
        return self.ems