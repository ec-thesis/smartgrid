import random

from ems.base import BaseEMS

# EMS that does just read/track a community, without any actions, but with special community energy prices.
class NoopEMS(BaseEMS):

    # Community efficiency: 100% if perfect equilibrium. 0% if pure export or pure import
    def get_community_equilibrium(self, community):
        if community['energyflow']['total_grid_power_rate'] == 0 or (community['energyflow']['total_grid_power_export_rate'] == 0 and community['energyflow']['total_grid_power_import_rate'] == 0):
            return 1
        # demand from grid / underproduction?
        elif community['energyflow']['total_grid_power_rate'] > 0 and community['energyflow']['total_grid_power_import_rate'] > 0:
            return (community['energyflow']['total_grid_power_import_rate'] - community['energyflow']['total_grid_power_rate']) / community['energyflow']['total_grid_power_import_rate']
        # excess / overproduction?
        elif community['energyflow']['total_grid_power_rate'] < 0 and community['energyflow']['total_grid_power_export_rate'] > 0:
            return (community['energyflow']['total_grid_power_export_rate'] + community['energyflow']['total_grid_power_rate']) / community['energyflow']['total_grid_power_export_rate']
        else:
            return 0

    # 100% if all member exports are sold solely to the community and not to central grid (underproduction). 0% if all sold to public grid (no member importing community)
    def get_export_coverage(self, community):
        # Community coverage/safety: 100% if covered or overproduction, 0% if demand and fully from grid
        if community['energyflow']['total_grid_power_rate'] >= 0 or community['energyflow']['total_grid_power_export_rate'] == 0:
            return 1.0  # underproduction
        else:
            return (community['energyflow']['total_grid_power_export_rate'] + community['energyflow']['total_grid_power_rate']) / community['energyflow']['total_grid_power_export_rate']

    # 100% if all member imports are covered by the community (overproduction), 0% if from central grid
    def get_import_coverage(self, community):
        # Community coverage/safety: 100% if covered or overproduction, 0% if demand and fully from grid
        if community['energyflow']['total_grid_power_rate'] <= 0 or community['energyflow']['total_grid_power_import_rate'] == 0:
            return 1.0  # overproduction
        else:
            return (community['energyflow']['total_grid_power_import_rate'] - community['energyflow']['total_grid_power_rate']) / community['energyflow']['total_grid_power_import_rate']
