import random

from ems.base import BaseEMS

# EMS that does not do anything but tracking. No community pricing benefits.
class IndividualEMS(BaseEMS):

    def get_community_equilibrium(self, community):
        return 0 # Always zero

    def get_export_coverage(self, community):
        return 0 # Always zero

    def get_import_coverage(self, community):
        return 0 # Always zero
