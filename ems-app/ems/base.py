from abc import abstractmethod, ABC
import threading
from tracking import EmsTracking
from datetime import datetime

lock_state = threading.RLock()

class BaseEMS(ABC):
    def __init__(self, config):
        self.config = config # ems config
        self.community = {} # initial community input / measurement
        self.tmp_state = {}
        self.prev_action = {}
        self.final_action = {}
        self.tracking = EmsTracking()

    @abstractmethod
    def optimize(self, community):
        pass

    @abstractmethod
    def get_community_equilibrium(self, community):
        pass

    @abstractmethod
    def get_export_coverage(self, community):
        pass

    @abstractmethod
    def get_import_coverage(self, community):
        pass

    def get_pricing(self):
        return {
            "community_import": 0.20 * 0.001,  # euro per Wh
            "community_export": 0.18 * 0.001,  # euro per Wh
        }

    def optimize(self, community):
        # Calculate action for current community staet (generate new state based on last state + current community measurment)
        self.action = {
            "timestamp": datetime.now().astimezone().isoformat(),
            "total_grid_power_rate": community['energyflow']['total_grid_power_rate'], # positive value = overproduction [W]
            "total_esr_target_grid_power_rate": 0,
            "efficiency_step": 0, # 100% if no central grid power required (equilibrium or overproduction)
            "actions": {},
            "scores": {}
        }

        return self.action

    # communitymeasurement is the state at timeslot t
    # emsactions are the actions to be applied for [t, t+15min]
    # communityresponse is the state measured at t+15min
    def finalize(self, communitymeasurement, emsactions, communityresponse):
        with lock_state:
            self.community = communityresponse
            self.prev_action = self.final_action
            self.final_action = emsactions
            self.tmp_state = {}
            self.tracking.store(self.community, self.final_action, self.get_pricing(), self.get_community_equilibrium(self.community), self.get_export_coverage(self.community), self.get_import_coverage(self.community))
            return self.final_action

    def get_state(self):
        with lock_state:
            return self.prev_action, self.final_action, self.community, self.tracking.getState()
        