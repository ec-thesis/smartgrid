import random
from datetime import datetime
import copy
from ems.noop import NoopEMS

# Simple EMS that operates on momentary power values only assigns ESR actions to consume/feedin energy based on ESR's SoC. Pricing inherited from noopEMS
class SumZeroEMS(NoopEMS):

    # Scoring function to determin a member's score ( chance) to be randomly selected
    def score_member(self, member, is_missing_power):
        # Depending if we discharge or charge, invert scoring
        if is_missing_power:
            # Omit ESR members that are already importing (e.g. due to low battery or manual battery charging from grid)
            if self.tmpcommunity[member]['energyflow']['grid_power_rate'] > 100:
                return 0

            return self.tmpcommunity[member]['energyflow']['battery_soc']
        else:
            # return 0 == don't tell any ESR to charge from EC excess, since it costs money (weekly performance (reward+equilibr. is still better if excess is compensated too!)
            return 1 - self.tmpcommunity[member]['energyflow']['battery_soc']

    # Returns the member by chance, based on its score, using monte carlo method random sampling
    def init_esr_member_scoring(self, members, is_missing_power):
        # Get score for every member
        for c in members:
            self.action['scores'][c] = self.score_member(c, is_missing_power)

    def choose_esr_member_to_allocate_power(self):
        scaled_scores = {}
        total_score = 0
        for c in self.action['scores']:
            total_score += self.action['scores'][c]

        # Scale score to sum 1.0
        if total_score > 0:
            prev_score = 0
            for c in self.action['scores']:
                scaled_scores[c] = self.action['scores'][c]/total_score + prev_score
                prev_score = scaled_scores[c]

        # Random sample
        sample = random.uniform(0, 1)

        # Get the member
        for c in scaled_scores:
            if scaled_scores[c] >= sample:
                return c


    def optimize(self, community):
        self.tmpcommunity = copy.deepcopy(community['details'])

        # Calculate action for current community staet (generate new state based on last state + current community measurment)
        self.action = {
            "timestamp": datetime.now().astimezone().isoformat(),
            "total_grid_power_rate": community['energyflow']['total_grid_power_rate'], # 0=equilibrium (island). negative=overproduction [W]
            "efficiency_step": 0, # 100% if all energy could be assigned to community
            "actions": {},
            "scores": {}
        }

        # Get amount of missing power that the EMS should equalize
        missing_power = community['energyflow']['total_grid_power_rate']

        # Compensate minimal constant loss of approx. 30 W per hour per member, due to inverter (would be drawn from central grid
        missing_power += 30 * len(community['all_members'])

        # Distribute excess or demand energy in the community over the ESR members
        max_rounds = 200
        assign_power = missing_power / max_rounds  # split missing power based on X rounds
        if len(community['online_esr']) > 0:
            self.init_esr_member_scoring(community['online_esr'], missing_power > 0)

            while abs(missing_power) > 10: # at least 0.01 kWh
                # Limit rounds
                max_rounds -= 1
                if max_rounds <= 0:
                    print("could not assign {} Wh due to members exhausted".format(missing_power))
                    break

                resulting_member = self.choose_esr_member_to_allocate_power()
                if not resulting_member:
                    print("could not assign {} Wh due to no available members".format(missing_power))
                    break

                # Skip members that already exceed limits, halve their score, lower assign_power
                if missing_power > 0:
                    if abs(self.tmpcommunity[resulting_member]['energyflow']['battery_power_rate'] + assign_power) > self.tmpcommunity[resulting_member]['energyflow']['battery_max_discharging_power_rate']:
                        self.action['scores'][resulting_member] /= 2
                        assign_power *= 0.95
                        continue
                    if abs(-self.tmpcommunity[resulting_member]['energyflow']['grid_power_rate'] + assign_power) > self.tmpcommunity[resulting_member]['energyflow']['grid_max_export_power_rate']:
                        self.action['scores'][resulting_member] /= 2
                        assign_power *= 0.95
                        continue
                else:
                    if abs(self.tmpcommunity[resulting_member]['energyflow']['battery_power_rate'] + assign_power) > self.tmpcommunity[resulting_member]['energyflow']['battery_max_charging_power_rate']:
                        self.action['scores'][resulting_member] /= 2
                        assign_power *= 0.95
                        continue
                    if abs(-self.tmpcommunity[resulting_member]['energyflow']['grid_power_rate'] + assign_power) > self.tmpcommunity[resulting_member]['energyflow']['grid_max_import_power_rate']:
                        self.action['scores'][resulting_member] /= 2
                        assign_power *= 0.95
                        continue

                # Set member action
                if not resulting_member in self.action['actions']:
                    self.action['actions'][resulting_member] = {"grid_import_ems_target": 0}

                # feedin is negative
                self.action['actions'][resulting_member]['grid_import_ems_target'] -= assign_power

                # Simulate the change in the member's energy flow to ensure we stay within the limits
                self.tmpcommunity[resulting_member]['energyflow']['battery_power_rate'] += assign_power
                self.tmpcommunity[resulting_member]['energyflow']['grid_power_rate'] -= assign_power

                # Round done
                missing_power -= assign_power

        # Calc efficiency that was achievable within this step
        self.action['efficiency_step'] = (abs(self.action['total_grid_power_rate']) + 1 - abs(missing_power)) / (abs(self.action['total_grid_power_rate']) + 1)

        return self.action
