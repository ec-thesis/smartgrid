""" Cloud EnergyManagementSystem (stateful) """

__author__ = "Stephan Podlipnig"

import json
import os
import asyncio

import httpx
from datetime import datetime
from flask import Flask, request
from threading import Event, Thread
from ems.factory import *

def load_config(default_config):
    try:
        param_config = json.loads(os.environ.get('EMS_CONFIG', "{}"))
        param_config['strategy'] = os.environ['EMS_STRATEGY']
        param_config['community'] = os.environ['COMMUNITY']
        return {**default_config, **param_config}
    except:
        raise ValidationError("Invalid or missing config json")


config = load_config({
    'timeout_sec': 10,
    'update_interval_sec': None,
    'community_url_schema': 'http://smartgridcommunity-{}-svc:8080/',
    'community': None,
    'strategy': None
})

app = Flask(__name__)
ems = EmsFactory(config['strategy'], config).getEms()


# https://stackoverflow.com/a/22498708
def call_repeatedly(interval, func, *args):
    stopped = Event()
    def loop():
        while not stopped.wait(interval):
            func(*args)
    Thread(target=loop).start()
    return stopped.set

def request_async(client, url, method='GET', json=None):
    # https://www.python-httpx.org/api/#Client
    return client.request(method, url, timeout=config['timeout_sec'], json=json)

async def query_community(timeslot, finalize=False):
    async with httpx.AsyncClient(http2=True) as client:
        url = ''.join([config['community_url_schema'].format(config['community']), "?timeslot=", timeslot])
        r = await asyncio.ensure_future(request_async(client, url, 'POST' if finalize else 'GET'))
        return json.loads(r.content)['data']

async def update_community(actions, timeslot):
    async with httpx.AsyncClient(http2=True) as client:
        url = ''.join([config['community_url_schema'].format(config['community']), "?timeslot=", timeslot])
        r = await asyncio.ensure_future(request_async(client, url, 'PUT', actions))
        return json.loads(r.content)['data']

def ems_step(timeslot=""):
    try:
        print("-------------------------------------")
        print(timeslot)
        print("1. Sampling community (GET):")
        community = asyncio.run(query_community(timeslot))
        print(community)

        print("2. Calculate EMS actions:")
        actions = ems.optimize(community)
        print(actions)

        # Propagate the actions to the community
        print("3. Apply actions (PUT):")
        asyncio.run(update_community(actions['actions'], timeslot))

        # Force query all members (must not fail)
        print("4. Simulation/tracking only: get finalized community state (POST):")
        response = asyncio.run(query_community(timeslot, True))

        # Track result
        ems.finalize(community, actions, response)

        # Return for simulationrunner
        print(response)
        return {"data": response}, 200
    except BaseException as error:
        print('An exception occurred: {}'.format(error))

def ems_get_state():
    state = ems.get_state()
    return {
        "data": {
            "ems": "ems-{}".format(config['community']),
            "strategy": config['strategy'],
            "community": config['community'],
            "timestamp": datetime.now().astimezone().isoformat(),
            "state": state[1],
            "details": state[2],
            "tracking": state[3]
        }
    }

# Start EMS loop
if config['update_interval_sec']:
    ems_cancler = call_repeatedly(config['update_interval_sec'], ems_step)


@app.route('/healthz')
def main():
    return 'OK\r\n', 200

# Manually trigger EMS calculation
@app.route('/', methods = ['GET'])
def trigger():
    timeslot = request.args.get('timeslot', default='', type=str)
    ems_step(timeslot)
    return ems_get_state()

if __name__ == "__main__":
    input = {'timestamp': '2023-03-26T16:17:00.627231+00:00', 'all_members': ['esr01', 'esr02', 'consumer03', 'consumer04'], 'online_members': ['esr01', 'esr02', 'consumer03', 'consumer04'], 'online_esr': ['esr01', 'esr02'], 'online_producers': ['esr01', 'esr02'], 'failed_members': [], 'energyflow': {'total_grid_power_rate': -100.58499999999549, 'total_grid_power_import_rate': 3718.0, 'total_grid_power_export_rate': 3818.5849999999955, 'total_grid_power_rate_nonesr': 3718.0, 'total_grid_power_import_rate_nonesr': 3718.0, 'total_grid_power_export_rate_nonesr': 0, 'total_grid_energy_import_within_timeslot': 929.5, 'total_grid_energy_export_within_timeslot': 954.6462499999989, 'total_grid_max_import_power_rate': 75300, 'total_grid_max_export_power_rate': 75300, 'total_consumption_power_rate': 7231.0, 'total_consumption_energy_within_timeslot': 1807.75, 'avg_per_member_grid_power_rate': -25.146249999998872, 'avg_per_member_grid_frequency': 50.0, 'total_production_power_rate': 12.0, 'total_max_production_power_rate': 0, 'total_production_energy_within_timeslot': 3.0, 'total_grid_power_rate_esr': -3818.5849999999955, 'total_grid_power_import_rate_esr': 0, 'total_grid_power_export_rate_esr': 3818.5849999999955, 'avg_battery_soc': 0.0748141651007008, 'total_battery_energy_available': 2344.2100706054835, 'total_max_battery_energy_controllable': 31000, 'total_battery_max_charging_power_rate': 19110.0, 'total_battery_max_discharging_power_rate': 19110.0}, 'details': {'esr01': {'energyflow': {'acc_selfconsumption_ratio': 0.9065369694203561, 'acc_selfconsumption_start_consumption_energy_absolute': 449733.0, 'acc_selfconsumption_start_grid_energy_imported_absolute': 21181.0, 'battery_energy_available': 915.8912026367254, 'battery_energy_change_relative': 1361.3364374999994, 'battery_max_charging_power_rate': 12110.0, 'battery_max_discharging_power_rate': 12110.0, 'battery_max_energy_available': 13200, 'battery_min_allowed_soc': 0.1, 'battery_power_rate': 5584.9699999999975, 'battery_soc': 0.0693856971694489, 'consumption_energy_absolute': 485884.5, 'consumption_energy_relative': 832.0, 'consumption_power_rate': 3328.0, 'grid_energy_exported_absolute': 1091049.2373242185, 'grid_energy_exported_relative': 567.2424999999994, 'grid_energy_imported_absolute': 24559.828749999997, 'grid_energy_imported_relative': 0, 'grid_frequency': 50.0, 'grid_import_ems_target': -2290.969999999997, 'grid_max_export_power_rate': 17300, 'grid_max_import_power_rate': 17300, 'grid_power_rate': -2268.9699999999975, 'max_production_power_rate': 0, 'production_energy_absolute': 1613942.0, 'production_energy_relative': 3.0, 'production_power_rate': 12.0}, 'is_available': True, 'is_esr': True, 'is_producer': 1, 'pricing': {'grid_export': 0.0001, 'grid_import': 0.0003}, 'site_name': 'esr01', 'site_uuid': 'esr01', 'timeslot': '2022-08-31T17:00:00'}, 'esr02': {'energyflow': {'acc_selfconsumption_ratio': 0.9530624144200772, 'acc_selfconsumption_start_consumption_energy_absolute': 9766586.0, 'acc_selfconsumption_start_grid_energy_imported_absolute': 10490.0, 'battery_energy_available': 1428.318867968758, 'battery_energy_change_relative': 422.8124062499995, 'battery_max_charging_power_rate': 7000.0, 'battery_max_discharging_power_rate': 7000.0, 'battery_max_energy_available': 17800, 'battery_min_allowed_soc': 0.1, 'battery_power_rate': 1734.614999999998, 'battery_soc': 0.0802426330319527, 'consumption_energy_absolute': 9788860.5, 'consumption_energy_relative': 46.25, 'consumption_power_rate': 185.0, 'grid_energy_exported_absolute': 680517.26471875, 'grid_energy_exported_relative': 387.4037499999995, 'grid_energy_imported_absolute': 11535.51124999999, 'grid_energy_imported_relative': 0, 'grid_frequency': 50.0, 'grid_import_ems_target': -1572.6149999999982, 'grid_max_export_power_rate': 10000, 'grid_max_import_power_rate': 10000, 'grid_power_rate': -1549.614999999998, 'max_production_power_rate': 0, 'production_energy_absolute': 11185395.0, 'production_energy_relative': 0.0, 'production_power_rate': 0.0}, 'is_available': True, 'is_esr': True, 'is_producer': 1, 'pricing': {'grid_export': 0.0001, 'grid_import': 0.0003}, 'site_name': 'esr02', 'site_uuid': 'esr02', 'timeslot': '2022-08-31T17:00:00'}, 'consumer03': {'energyflow': {'acc_selfconsumption_ratio': 0, 'acc_selfconsumption_start_consumption_energy_absolute': 3005161.0, 'acc_selfconsumption_start_grid_energy_imported_absolute': 290773.0, 'battery_energy_available': 0, 'battery_energy_change_relative': 0, 'battery_max_charging_power_rate': 0, 'battery_max_discharging_power_rate': 0, 'battery_max_energy_available': 0, 'battery_min_allowed_soc': 0.1, 'battery_power_rate': 0.0, 'battery_soc': 0, 'consumption_energy_absolute': 3087557.75, 'consumption_energy_relative': 67.25, 'consumption_power_rate': 269.0, 'grid_energy_exported_absolute': 1616312.0, 'grid_energy_exported_relative': 0, 'grid_energy_imported_absolute': 373169.75, 'grid_energy_imported_relative': 67.25, 'grid_frequency': 50.0, 'grid_import_ems_target': 0, 'grid_max_export_power_rate': 24000, 'grid_max_import_power_rate': 24000, 'grid_power_rate': 269.0, 'max_production_power_rate': 0, 'production_energy_absolute': 4646100.0, 'production_energy_relative': 0, 'production_power_rate': 0}, 'is_available': True, 'is_esr': False, 'is_producer': 0, 'pricing': {'grid_export': 0.0001, 'grid_import': 0.0003}, 'site_name': 'consumer03', 'site_uuid': 'consumer03', 'timeslot': '2022-08-31T17:00:00'}, 'consumer04': {'energyflow': {'acc_selfconsumption_ratio': 0, 'acc_selfconsumption_start_consumption_energy_absolute': 9765348.0, 'acc_selfconsumption_start_grid_energy_imported_absolute': 1514763.0, 'battery_energy_available': 0, 'battery_energy_change_relative': 0, 'battery_max_charging_power_rate': 0, 'battery_max_discharging_power_rate': 0, 'battery_max_energy_available': 0, 'battery_min_allowed_soc': 0.1, 'battery_power_rate': 0.0, 'battery_soc': 0, 'consumption_energy_absolute': 9933457.5, 'consumption_energy_relative': 862.25, 'consumption_power_rate': 3449.0, 'grid_energy_exported_absolute': 54215.0, 'grid_energy_exported_relative': 0, 'grid_energy_imported_absolute': 1682872.5, 'grid_energy_imported_relative': 862.25, 'grid_frequency': 50.0, 'grid_import_ems_target': 0, 'grid_max_export_power_rate': 24000, 'grid_max_import_power_rate': 24000, 'grid_power_rate': 3449.0, 'max_production_power_rate': 0, 'production_energy_absolute': 8618900.0, 'production_energy_relative': 0, 'production_power_rate': 0}, 'is_available': True, 'is_esr': False, 'is_producer': 0, 'pricing': {'grid_export': 0.0001, 'grid_import': 0.0003}, 'site_name': 'consumer04', 'site_uuid': 'consumer04', 'timeslot': '2022-08-31T17:00:00'}}, 'online_member_rate': 1.0, 'online_producer_rate': 0.5, 'online_esr_rate': 0.5}
    res = ems.optimize(input)
    ems.finalize(input, res, input)
    print(ems_get_state())
