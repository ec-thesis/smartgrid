from datetime import datetime


class EmsTracking:
    def __init__(self):
        self.resetState()

    def store(self, community, state, pricing, community_equilib, export_coverage, import_coverage):
        self.state['steps'] = self.state['steps'] + 1

        if not community or not 'details' in community:
            return

        total_momentary_grid_eur = 0
        total_momentary_community_eur = 0

        for agent, data in community['details'].items():
            # Init
            if agent not in self.state['details']:
                self.state['details'][agent] = {
                    "startTime": datetime.now().astimezone().isoformat(),
                    "steps": 0,
                    "initValues": data,
                    "cumulativeValues": {
                        "gridimported_energy": 0, # Wh
                        "gridexported_energy": 0, # Wh
                        "communityimported_energy": 0,  # Wh
                        "communityexported_energy": 0,  # Wh
                        "produced_energy": 0, # Wh
                        "consumed_energy": 0,  # Wh
                        "community_equilib": 0,
                        "grid_cost": 0.0,  # euro
                        "grid_reward": 0.0,  # euro
                        "community_cost": 0.0, # euro
                        "community_reward": 0.0, # euro
                    },
                }

            # community agent sums (backpropagating community total proportionally to single agents)

            # Separating energy flow: either from/to central grid or distributed within the community
            gridimported =   (1-import_coverage) * data['energyflow']['grid_energy_imported_relative']
            communityimported = import_coverage  * data['energyflow']['grid_energy_imported_relative']
            gridexported =   (1-export_coverage) * data['energyflow']['grid_energy_exported_relative']
            communityexported = export_coverage  * data['energyflow']['grid_energy_exported_relative']
            self.state['details'][agent]['cumulativeValues']['gridimported_energy'] += gridimported
            self.state['details'][agent]['cumulativeValues']['gridexported_energy'] += gridexported
            self.state['details'][agent]['cumulativeValues']['communityimported_energy'] += communityimported
            self.state['details'][agent]['cumulativeValues']['communityexported_energy'] += communityexported
            self.state['details'][agent]['cumulativeValues']['produced_energy'] += data['energyflow']['production_energy_relative']
            self.state['details'][agent]['cumulativeValues']['consumed_energy'] += data['energyflow']['consumption_energy_relative']
            self.state['details'][agent]['cumulativeValues']['energy_balance'] = self.state['details'][agent]['cumulativeValues']['produced_energy'] - self.state['details'][agent]['cumulativeValues']['consumed_energy']

            grid_cost        = gridimported * data['pricing']['grid_import']
            grid_reward      = gridexported * data['pricing']['grid_export']
            total_momentary_grid_eur += grid_reward - grid_cost
            community_cost   = communityimported * pricing['community_import']
            community_reward = communityexported * pricing['community_export']
            total_momentary_community_eur += community_reward - community_cost
            self.state['details'][agent]['cumulativeValues']['grid_cost']        += grid_cost
            self.state['details'][agent]['cumulativeValues']['grid_reward']      += grid_reward
            self.state['details'][agent]['cumulativeValues']['community_cost']   += community_cost
            self.state['details'][agent]['cumulativeValues']['community_reward'] += community_reward

            # Cummulative total sums (no negative numbers here)
            self.state['totals']['gridimported_energy'] += gridimported
            self.state['totals']['gridexported_energy'] += gridexported
            self.state['totals']['communityimported_energy'] += communityimported
            self.state['totals']['communityexported_energy'] += communityexported
            self.state['totals']['produced_energy']  += data['energyflow']['production_energy_relative']
            self.state['totals']['consumed_energy']  += data['energyflow']['consumption_energy_relative']
            self.state['totals']['grid_cost']        += grid_cost
            self.state['totals']['grid_reward']      += grid_reward
            self.state['totals']['community_cost']   += community_cost
            self.state['totals']['community_reward'] += community_reward

        self.state['totals']['tmp_steps']                  += 1
        self.state['totals']['tmp_cum_community_equilib']  += community_equilib
        self.state['totals']['community_equilib']      = self.state['totals']['tmp_cum_community_equilib'] / self.state['totals']['tmp_steps']
        self.state['totals']['community_independency'] = self.state['totals']['communityimported_energy'] / (1 + self.state['totals']['communityimported_energy'] + self.state['totals']['gridimported_energy'])
        self.state['totals']['community_autarky']      = 1 - (self.state['totals']['gridimported_energy'] / (1 + self.state['totals']['consumed_energy']))
        self.state['totals']['balance_eur']            = self.state['totals']['grid_reward'] + self.state['totals']['community_reward'] - self.state['totals']['grid_cost'] - self.state['totals']['community_cost']
        self.state['totals']['tmp_cum_community_battery_soc'] += community['energyflow']['avg_battery_soc']
        self.state['totals']['community_battery_soc']   = self.state['totals']['tmp_cum_community_battery_soc'] / self.state['totals']['tmp_steps']
        self.state['totals']['tmp_cum_community_onlinerate']  += community['online_member_rate']
        self.state['totals']['community_onlinerate']    = self.state['totals']['tmp_cum_community_onlinerate'] / self.state['totals']['tmp_steps']
        self.state['totals']['community_esr_capacity']  = community['energyflow']['total_max_battery_energy_controllable']

        # Momentary
        self.state['totals']['community_equilib_momentary']     = community_equilib
        self.state['totals']['community_power_rate_among_members'] = community_equilib * community['energyflow']['total_grid_power_rate']
        self.state['totals']['community_power_import_rate_among_members'] = import_coverage * community['energyflow']['total_grid_power_import_rate']
        self.state['totals']['community_power_export_rate_among_members'] = export_coverage * community['energyflow']['total_grid_power_export_rate']
        self.state['totals']['grid_balance_eur_momentary']      = total_momentary_grid_eur
        self.state['totals']['community_balance_eur_momentary'] = total_momentary_community_eur
        self.state['totals']['community_battery_soc_momentary'] = community['energyflow']['avg_battery_soc']

        self.state['totals']['price_grid_export'] = data['pricing']['grid_export']
        self.state['totals']['price_grid_import'] = data['pricing']['grid_import']
        self.state['totals']['price_community_export'] = pricing['community_export']
        self.state['totals']['price_community_import'] = pricing['community_import']

        # Theoretical max. reward: After whole week: get energy saldo per agent. remove minimal redistribution cost
        total_missing_agent_energy = 0
        total_excess_agent_energy = 0
        for agent, data in community['details'].items():
            if self.state['details'][agent]['cumulativeValues']['energy_balance'] > 0:
                total_excess_agent_energy += self.state['details'][agent]['cumulativeValues']['energy_balance']
            else:
                total_missing_agent_energy += -self.state['details'][agent]['cumulativeValues']['energy_balance']

        total_ec_saldo = self.state['totals']['produced_energy'] - self.state['totals']['consumed_energy']
        total_redistribution_energy = min(max(0, total_ec_saldo), total_missing_agent_energy)
        total_grid_energy = total_ec_saldo - total_redistribution_energy

        total_optimal_balance = 0
        if total_grid_energy > 0:
            total_optimal_balance = total_grid_energy * data['pricing']['grid_export']
        else:
            total_optimal_balance = total_grid_energy * data['pricing']['grid_import']

        total_optimal_balance -= total_redistribution_energy * (pricing['community_import']-pricing['community_export'])

        self.state['totals']['optimal_balance'] = total_optimal_balance
        self.state['totals']['total_missing_agent_energy'] = total_missing_agent_energy
        self.state['totals']['total_excess_agent_energy'] = total_excess_agent_energy
        self.state['totals']['total_ec_saldo'] = total_ec_saldo
        self.state['totals']['total_redistribution_energy'] = total_redistribution_energy
        self.state['totals']['total_grid_energy'] = total_grid_energy

        # Finding (given fixed energy prices): auch wenn man noch so viel forecasting macht, es bringt nicht mehr viel und das SumZero+LS4H performed schon super!

        # The finding the optimal EC can be reduced to a slot assignment problem


        return self

    def resetState(self):
        self.state = {
            "steps": 0,
            "details": {},
            "totals": {
                "gridimported_energy": 0, # Wh (from central grid)
                "gridexported_energy": 0, # Wh (to central grid)
                "communityimported_energy": 0,  # Wh (from community)
                "communityexported_energy": 0,  # Wh (to community)
                "produced_energy": 0,
                "consumed_energy": 0,
                "grid_cost": 0.0,  # euro (from central grid)
                "grid_reward": 0.0,  # euro (to central grid)
                "community_onlinerate": 0.0,
                "community_cost": 0.0, # euro (from community)
                "community_reward": 0.0, # euro (to community)
                "community_equilib_momentary": 0,
                "community_equilib": 0,
                "community_independency": 0, # independency from central grid (regarding energy import coverage)
                "community_battery_soc": 0,
                "community_battery_soc_momentary": 0,
                "tmp_cum_community_equilib": 0,
                "tmp_cum_community_onlinerate": 0,
                "tmp_cum_community_battery_soc": 0,
                "tmp_steps": 0
            },
        }

    def getState(self):
        return self.state