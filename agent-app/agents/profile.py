import random
import json
from time import strptime

from exceptions import ValidationError


def mix(val1, val2, val2_amount=0.25):
    return round(val1 * (1-val2_amount) + val2 * val2_amount)

def rand(max=1000, min=0):
    return round(random.uniform(min, max))

# Consumption + PV production is obtained by file
# Grid + Battery i/o is calculated on-the-fly
class ProfileAgent:
    def __init__(self, config):
        self.config = config

        with open(self.config['profile_basefolder'] + '/' + self.config['profile'], 'r') as f:
            self.profile = json.load(f)['data']

        self.behaviors = None
        if self.config['simulation_fault_injection']:
            with open(self.config['profile_basefolder'] + '/faults/' + self.config['simulation_fault_injection'] + ".json", 'r') as f:
                self.behaviors = json.load(f)[self.config['profile'].replace(".energyflow.json", "")]

        # Initial values
        self.agent = {
            "timeslot": self.profile[0]['time'].replace(" ", "T"),  # used for 15min relative energy tracking
        }
        self.agent = {
            "site_uuid": self.config['site_uuid'],
            "site_name": self.config['site_name'],
            "timeslot": self.profile[0]['time'].replace(" ", "T"), # used for 15min relative energy tracking
            "is_available": True,  # used to simulate "soft faults" to able to track energy prod/consumption for final results
            "is_producer": self.config['is_producer'],
            "is_esr": self.config['net_esr_capacity_wh'] > 0,
            "energyflow": {
                # consumer
                "grid_energy_imported_absolute": self.getValue('ENERGY_IMPORTED'), # Wh
                "grid_energy_imported_relative": 0, # Wh in given 15min timeslot
                "grid_power_rate": 0, # W, positive=consumption
                "consumption_power_rate": 0, # W. Actual consumption of the agent (ignoring battery powers). If pure consumer, this equals grid_power_rate. Always positive
                "consumption_energy_absolute": self.getValue('ENERGY_CONSUMED_CALC'),  # Wh, calculated on-the-fly only
                "consumption_energy_relative": 0,  # Wh, calculated on-the-fly only
                "grid_max_import_power_rate": self.config['max_grid_power_rate_w'], # W, always positive.
                "grid_frequency": 50.0, # Hz

                # producer OR ESR
                "grid_energy_exported_absolute": self.getValue('ENERGY_EXPORTED'),  # Wh
                "grid_energy_exported_relative": 0,  # Wh in given 15min timeslot
                "grid_max_export_power_rate": self.config['max_grid_power_rate_w'],  # W, always positive. the max possible power rate for grid export (reduced e.g. on self consumption from battery)
                "acc_selfconsumption_start_consumption_energy_absolute": self.getValue('ENERGY_CONSUMED_CALC'),
                "acc_selfconsumption_start_grid_energy_imported_absolute": self.getValue('ENERGY_IMPORTED'),
                "acc_selfconsumption_ratio": 0, # 0..1 % ratio how much of the current consumption power is not obtained from grid

                # producer (has PV)
                "production_energy_absolute": self.getValue('ENERGY_PRODUCED'),  # Wh,  calculated on-the-fly only
                "production_energy_relative": 0,  # Wh,  calculated on-the-fly only
                "production_power_rate": 0, # W, if PV
                "max_production_power_rate": 0, # Wp, if PV. currently unused

                # ESR
                #grid_import_ems_target is the only controllable param!
                "grid_import_ems_target": 0.0, # W, negative=feedin. currently set value of the EMS performance power rate that the agent should import from the grid

                "battery_soc": 0, # 0..1
                "battery_power_rate": 0.0, # W, positive=discharging. the current power rate i/o of the battery, calculated on-the-fly only
                "battery_energy_change_relative": 0,  # Wh, how much the battery energy changed (positive=discharged). calculated on-the-fly only
                "battery_max_charging_power_rate": 0, # W, always positive
                "battery_max_discharging_power_rate": 0, # W, always positive
                "battery_max_energy_available": 0, # Wh, max (net) reserved energy for community if fully charged
                "battery_min_allowed_soc": 0.1, # Absolute minimum of SoC allowed: 10% per default
                "battery_energy_available": 0 # Wh, available energy for community, calculated on-the-fly only
            },
            "pricing": {
                "grid_import": 0.30 * 0.001, # euro per Wh
                "grid_export": 0.10 * 0.001, # euro per Wh
            }
        }

        if self.agent['is_esr']:
            self.agent['energyflow']['battery_soc'] = self.getValue('STATE_OF_CHARGE', 50) / 100
            self.agent['energyflow']['battery_max_charging_power_rate'] = 0.7 * self.config['max_grid_power_rate_w'] # W, always positive. assuming full charge possible in 2 hours.
            self.agent['energyflow']['battery_max_discharging_power_rate'] = 0.7 * self.config['max_grid_power_rate_w'] # W, always positive. assuming full discharge possible in 2 hours.
            self.agent['energyflow']['battery_max_energy_available'] = self.config['net_esr_capacity_wh']
            #self.agent['energyflow']['battery_min_allowed_soc'] = max(self.agent['energyflow']['battery_min_allowed_soc'], 1 - self.config['net_esr_usable_above_percent'])
            self.agent['energyflow']['battery_energy_available'] = self.agent['energyflow']['battery_soc'] * self.agent['energyflow']['battery_max_energy_available']

        if self.agent['is_producer']:
            self.agent['energyflow']['production_power_rate'] = self.getValue('POWER_PRODUCTION')

    # Return single value from profile based on current 15min timeslot
    def getValue(self, valueKey, fallbackValue=0, override_timeslot=None):
        if self.agent and (override_timeslot or (self.agent.get('timeslot') and self.agent['timeslot'])):
            if override_timeslot:
                timeslot = override_timeslot
            else:
                timeslot = self.agent['timeslot']

            for entry in self.profile:
                if entry['value_key'] == valueKey and entry['time']:
                    # if strptime(entry['time'], '%Y-%m-%d %H:%M:%S') == self.agent['timeslot']:
                    if entry['time'].replace("T", " ") == timeslot.replace("T", " "):
                        return entry['round']

        return fallbackValue

    # On finalize, the absolute counters are incremented too (must be done only once per timestep)
    def step(self, timeslot, finalize=False):
        if finalize and self.agent['timeslot'] != timeslot:
            raise ValidationError("Disallowed timeslot change: "+self.agent['timeslot']+" to "+timeslot)

        # reset EMS modifier on timeslot change
        if self.agent['timeslot'] != timeslot:
            self.agent['energyflow']['grid_import_ems_target'] = 0
        self.agent['timeslot'] = timeslot
        self.agent['is_available'] = True if self.get_behavior() == "online" else False

        # reset relative energy (calculated now)
        self.agent['energyflow']['grid_energy_exported_relative'] = 0
        self.agent['energyflow']['grid_energy_imported_relative'] = 0
        self.agent['energyflow']['consumption_energy_relative'] = 0
        self.agent['energyflow']['production_energy_relative'] = 0
        self.agent['energyflow']['battery_energy_change_relative'] = 0

        # Consumption + PV production profile is obtained by file only
        self.agent['energyflow']['consumption_power_rate'] = self.casestudy3_modifier(timeslot, self.getValue('POWER_CONSUMPTION_CALC'))
        if finalize: self.agent['energyflow']['consumption_energy_absolute'] += self.integrate_power_over_step_interval(self.agent['energyflow']['consumption_power_rate'])
        self.agent['energyflow']['consumption_energy_relative'] = self.integrate_power_over_step_interval(self.agent['energyflow']['consumption_power_rate'])
        if self.agent['is_producer']:
            self.agent['energyflow']['production_power_rate'] = self.getValue('POWER_PRODUCTION')
            if finalize: self.agent['energyflow']['production_energy_absolute'] += self.integrate_power_over_step_interval(self.agent['energyflow']['production_power_rate'])
            self.agent['energyflow']['production_energy_relative'] = self.integrate_power_over_step_interval(self.agent['energyflow']['production_power_rate'])

        self.update_grid_power_rate()

        # simulate battery behavior / control battery power rate. if PV not covering the demand, prefer_battery as long as SoC > 10%.
        if self.agent['is_esr']:
            target_battery_power_rate = 0
            # get demand / excess based on current consumption (grid_import_ems_target treated as extra consumption/production)
            remainder = self.agent['energyflow']['production_power_rate'] - self.agent['energyflow']['consumption_power_rate'] + self.agent['energyflow']['grid_import_ems_target']
            self.agent['energyflow']['remainder'] = remainder
            # try to resolve remainder (to 0) via battery_power_rate
            if remainder > 0:
                # in case of excess: charge or feed in?
                if self.config['simulation_excess_strategy'] == "prefer_battery" and self.agent['energyflow']['battery_soc'] < 0.99:
                    target_battery_power_rate = - min(remainder, self.agent['energyflow']['battery_max_charging_power_rate'])
            if remainder < 0:
                if self.config['simulation_covering_strategy'] == "prefer_battery" and self.agent['energyflow']['battery_soc'] > self.agent['energyflow']['battery_min_allowed_soc']:
                    target_battery_power_rate = min(-remainder, self.agent['energyflow']['battery_max_discharging_power_rate'])

            # Update battery_energy_available based on target charging/discharging rate and battery energy bounds
            batt = self.agent['energyflow']['battery_energy_available']
            batt_new = batt - self.integrate_power_over_step_interval( target_battery_power_rate )
            batt_new = max(0, batt_new)
            batt_new = min(batt_new, self.agent['energyflow']['battery_max_energy_available'])
            self.agent['energyflow']['battery_energy_change_relative'] = batt - batt_new

            # Adjust battery_power_rate based on actual battery energy change after taking into account the limits
            self.agent['energyflow']['battery_power_rate'] = self.agent['energyflow']['battery_energy_change_relative'] * 4

            # Efficiency factor 95% for charge/discharge operations (2,5% each)
            loss = abs(self.agent['energyflow']['battery_energy_change_relative']) * 0.025
            batt_new -= loss
            self.agent['energyflow']['battery_energy_change_relative'] -= loss

            if finalize: self.agent['energyflow']['battery_energy_available'] = batt_new

            self.update_grid_power_rate()

        # Update relative import/export based on current power rate
        if self.agent['energyflow']['grid_power_rate'] > 0:
            self.agent['energyflow']['grid_energy_imported_relative'] = self.integrate_power_over_step_interval(self.agent['energyflow']['grid_power_rate'])
            if finalize: self.agent['energyflow']['grid_energy_imported_absolute'] += self.agent['energyflow']['grid_energy_imported_relative']
        else:
            self.agent['energyflow']['grid_energy_exported_relative'] = self.integrate_power_over_step_interval(abs(self.agent['energyflow']['grid_power_rate']))
            if finalize: self.agent['energyflow']['grid_energy_exported_absolute'] += self.agent['energyflow']['grid_energy_exported_relative']

        if self.agent['is_esr']:
            # Update SoC
            self.agent['energyflow']['battery_soc'] = self.agent['energyflow']['battery_energy_available'] / self.agent['energyflow']['battery_max_energy_available']
            if self.agent['energyflow']['battery_soc'] > 1:
                self.agent['energyflow']['battery_soc'] = 1

        # Calculate selfconsumption rate = autarky
        self.agent['energyflow']['acc_selfconsumption_ratio'] = 1 - min(1, max(0, (self.agent['energyflow']['grid_energy_imported_absolute'] - self.agent['energyflow']['acc_selfconsumption_start_grid_energy_imported_absolute']) / max(1, self.agent['energyflow']['consumption_energy_absolute'] - self.agent['energyflow']['acc_selfconsumption_start_consumption_energy_absolute'])))

        return self.agent

    def update_grid_power_rate(self):
        # calculate energy flow (grid_power_rate)
        self.agent['energyflow']['grid_power_rate'] = self.agent['energyflow']['consumption_power_rate'] - self.agent['energyflow']['production_power_rate'] - self.agent['energyflow']['battery_power_rate']

    def integrate_power_over_step_interval(self, value):
        # one step every 15min
        return value / 4

    def get_behavior(self):
        if not self.behaviors or not self.agent['timeslot']:
            return "online"

        for entry in self.behaviors:
            if entry['time']:
                if entry['time'].replace("T", " ") == self.agent['timeslot'].replace("T", " "):
                    return entry['behavior']

        return "timeout"  # fallback

    # Special behavior, used for CS3 simulation only
    def casestudy3_modifier(self, timeslot, consumption):

        if self.config['simulation_load_shifting'] == "SumZeroWithAllShift50perc4h" or (self.config['simulation_load_shifting'] == "SumZeroWithConsShift50perc4h" and not self.agent['is_esr']):
            # Shift 50% of power from 16:00-20:00 => 12:00-16:00
            if 'T12:' in timeslot:
                consumption += self.getValue('POWER_CONSUMPTION_CALC', 0, timeslot.replace('T12:', 'T16:')) * 0.5
            if 'T13:' in timeslot:
                consumption += self.getValue('POWER_CONSUMPTION_CALC', 0, timeslot.replace('T13:', 'T17:')) * 0.5
            if 'T14:' in timeslot:
                consumption += self.getValue('POWER_CONSUMPTION_CALC', 0, timeslot.replace('T14:', 'T18:')) * 0.5
            if 'T15:' in timeslot:
                consumption += self.getValue('POWER_CONSUMPTION_CALC', 0, timeslot.replace('T15:', 'T19:')) * 0.5
            if 'T16:' in timeslot:
                consumption = consumption * 0.5
            if 'T17:' in timeslot:
                consumption = consumption * 0.5
            if 'T18:' in timeslot:
                consumption = consumption * 0.5
            if 'T19:' in timeslot:
                consumption = consumption * 0.5
        elif self.config['simulation_load_shifting'] == "SumZeroWithAllShift50perc2h" or (self.config['simulation_load_shifting'] == "SumZeroWithConsShift50perc2h" and not self.agent['is_esr']):
            # Shift 50% of power from 17:00-19:00 => 13:00-15:00
            if 'T13:' in timeslot:
                consumption += self.getValue('POWER_CONSUMPTION_CALC', 0, timeslot.replace('T13:', 'T17:')) * 0.5
            if 'T14:' in timeslot:
                consumption += self.getValue('POWER_CONSUMPTION_CALC', 0, timeslot.replace('T14:', 'T18:')) * 0.5
            if 'T17:' in timeslot:
                consumption = consumption * 0.5
            if 'T18:' in timeslot:
                consumption = consumption * 0.5

        return consumption

    def get_state(self):
        return self

    # update state (apply actions) for the interval [timeslot, timeslot+15min]
    def update_state(self, update, timeslot=""):
        if self.agent['timeslot'] != timeslot:
            raise ValidationError("Disallowed timeslot change: "+self.agent['timeslot']+" to "+timeslot)

        # Set action from EMS
        if update and "grid_import_ems_target" in update:
            self.agent['energyflow']['grid_import_ems_target'] = update['grid_import_ems_target']
        else:
            self.agent['energyflow']['grid_import_ems_target'] = 0

        return self.step(timeslot)

