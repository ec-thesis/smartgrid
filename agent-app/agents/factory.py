import json
from agents.simulation import SimulationAgent
from agents.profile import ProfileAgent
from exceptions import *


class AgentFactory:
    def __init__(self, type, config):
        self.config = config

        try:
            agenttypes = {
                "simulation": SimulationAgent,
                "profile": ProfileAgent,
                #"ntuity": NtuityAgent,
                #"api": ApiAgent,
            }
            self.agent = agenttypes[type](self.config)
        except:
            raise ValidationError("Invalid agent type or config")

    def getAgent(self):
        return self.agent