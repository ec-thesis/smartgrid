import random
from datetime import datetime
from threading import Event, Thread

# https://stackoverflow.com/a/22498708
def call_repeatedly(interval, func, *args):
    stopped = Event()
    def loop():
        while not stopped.wait(interval): # the first call is in `interval` secs
            func(*args)
    Thread(target=loop).start()    
    return stopped.set

def mix(val1, val2, val2_amount=0.25):
    return round(val1 * (1-val2_amount) + val2 * val2_amount)

def rand(max=1000, min=0):
    return round(random.uniform(min, max))

class SimulationAgent:
    def __init__(self, config):
        self.config = config

        self.sim = {
            "site_uuid": "ea46a61e-16e6-46b9-a62f-251625642999",
            "site_name": "TZ Freistadt",
            "timeslot": "", # used for 15min relative energy tracking
            "is_available": True,
            "is_producer": random.uniform(0, 1) > 0.33, # the less producers, the faster the EC energy is exhausted
            "is_esr": random.uniform(0, 1) > 0.66,
            "energyflow": {
                # consumer
                "grid_energy_imported_absolute": rand(5000000), # Wh
                "grid_energy_imported_relative": 0, # Wh in given 15min timeslot
                "grid_power_rate": 0, # W, positive=consumption
                "consumption_power_rate": 0, # W. If pure consumer, this equals grid_power_rate
                "consumption_energy_absolute": 0,  # Wh, calculated on-the-fly only
                "consumption_energy_relative": 0,  # Wh, calculated on-the-fly only
                "grid_max_import_power_rate": rand(5000, 30000), # W, always positive.
                "grid_frequency": random.uniform(49.0, 51.0), # Hz

                # producer OR ESR
                "grid_energy_exported_absolute": 0,  # Wh
                "grid_energy_exported_relative": 0,  # Wh in given 15min timeslot
                "grid_max_export_power_rate": 0,  # W, always positive. the max possible power rate for grid export (reduced e.g. on self consumption from battery)

                "acc_selfconsumption_start_consumption_energy_absolute": 0,
                "acc_selfconsumption_start_grid_energy_imported_absolute": 0,
                "acc_selfconsumption_ratio": 0,  # 0..1 % ratio how much of the current consumption power is not obtained from grid


                # producer (has PV)
                "production_energy_absolute": 0,  # Wh,  calculated on-the-fly only
                "production_energy_relative": 0,  # Wh,  calculated on-the-fly only
                "production_power_rate": 0, # W, if PV
                "max_production_power_rate": 0, # Wp, if PV

                # ESR
                #grid_import_ems_target is the only controllable param!
                "grid_import_ems_target": 0.0, # W, negative=feedin. currently set value of the performance power rate that the agent should import from the grid

                "battery_soc": 0, # 0..1
                "battery_power_rate": 0.0, # W, positive=discharging. the current power rate i/o of the battery
                "battery_energy_change_relative": 0,  # Wh, how much the battery energy changed (positive=discharged). calculated on-the-fly only
                "battery_max_charging_power_rate": 0, # W, always positive
                "battery_max_discharging_power_rate": 0, # W, always positive
                "battery_max_energy_available": 0, # Wh, max reserved energy for community if 100% charged
                "battery_energy_available": 0 # Wh, available energy for community


            },
            "pricing": {
                "grid_import": 0.25 * 0.001,  # euro per Wh
                "grid_export": 0.05 * 0.001,  # euro per Wh
            }
        }

        # Set other static values based on init values
        if self.sim['is_producer']:
            self.sim['energyflow']['production_power_rate'] = rand(10000)
            self.sim['energyflow']['max_production_power_rate'] = 2.0 * self.sim['energyflow']['production_power_rate']

        if self.sim['is_esr']:
            self.sim['energyflow']['battery_power_rate'] = rand(2000, 10000)
            self.sim['energyflow']['battery_max_charging_power_rate'] = 1.2 * self.sim['energyflow']['battery_power_rate']
            self.sim['energyflow']['battery_max_discharging_power_rate'] = 2.0 * self.sim['energyflow']['battery_power_rate']
            self.sim['energyflow']['battery_max_energy_available'] = rand(100000)
            self.sim['energyflow']['battery_energy_available'] = random.uniform(0.1, 0.7) * self.sim['energyflow']['battery_max_energy_available']

        if self.sim['is_producer'] or self.sim['is_esr']:
            self.sim['energyflow']['grid_energy_exported_absolute'] = rand(1000000)
            self.sim['energyflow']['grid_max_export_power_rate'] = rand(5000, 20000)

        call_repeatedly(self.config['simulation_update_interval_sec'], self.step)

    def step(self, _=""):
        self.sim['timestamp'] = datetime.now().astimezone().isoformat()
        self.sim['energyflow']['grid_energy_exported_relative'] = 0
        self.sim['energyflow']['grid_energy_imported_relative'] = 0
        self.sim['timeslot'] = self.sim['timestamp']

        self.update_grid_power_rate()

        # randomize momentary PV + consumption power rate
        self.sim['energyflow']['consumption_power_rate'] = mix(self.sim['energyflow']['consumption_power_rate'], rand(3333))
        if self.sim['is_producer']:
            self.sim['energyflow']['production_power_rate'] = mix(self.sim['energyflow']['production_power_rate'], rand(10000))

        self.update_grid_power_rate()

        # simulate battery / control battery power rate
        if self.sim['is_esr']:
            self.sim['energyflow']['battery_power_rate'] = 0 # reset
            # get current demand / excess
            excess = self.sim['energyflow']['production_power_rate'] - self.sim['energyflow']['consumption_power_rate'] + self.sim['energyflow']['grid_import_ems_target']
            # in case of excess: charge or feed in?
            if excess > 0:
                if self.config['simulation_excess_strategy'] == "prefer_battery" and self.sim['energyflow']['battery_soc'] < 0.95:
                    self.sim['energyflow']['battery_power_rate'] = - min(excess, self.sim['energyflow']['battery_max_charging_power_rate'])
            if excess < 0:
                if self.config['simulation_covering_strategy'] == "prefer_battery" and self.sim['energyflow']['battery_soc'] > 0.05:
                    self.sim['energyflow']['battery_power_rate'] = min(-excess, self.sim['energyflow']['battery_max_discharging_power_rate'])

        self.update_grid_power_rate()

        # Update relative import/export based on current power rate
        if self.sim['energyflow']['grid_power_rate'] > 0:
            self.sim['energyflow']['grid_energy_imported_relative'] += self.integrate_power_over_step_interval(self.sim['energyflow']['grid_power_rate'])
            self.sim['energyflow']['grid_energy_imported_absolute'] += self.integrate_power_over_step_interval(self.sim['energyflow']['grid_power_rate'])
        else:
            self.sim['energyflow']['grid_energy_exported_relative'] += self.integrate_power_over_step_interval(abs(self.sim['energyflow']['grid_power_rate']))
            self.sim['energyflow']['grid_energy_exported_absolute'] += self.integrate_power_over_step_interval(abs(self.sim['energyflow']['grid_power_rate']))

        # Update battery energy
        if self.sim['is_esr']:
            # Update battery_energy_available based on current charging/discharging rate
            self.sim['energyflow']['battery_energy_available'] -= self.integrate_power_over_step_interval(self.sim['energyflow']['battery_power_rate'])
            self.sim['energyflow']['battery_energy_available'] = max(0, self.sim['energyflow']['battery_energy_available'])
            self.sim['energyflow']['battery_energy_available'] = min(self.sim['energyflow']['battery_energy_available'], self.sim['energyflow']['battery_max_energy_available'])

            # Update SoC
            self.sim['energyflow']['battery_soc'] = self.sim['energyflow']['battery_energy_available'] / self.sim['energyflow']['battery_max_energy_available']
            if self.sim['energyflow']['battery_soc'] > 1:
                self.sim['energyflow']['battery_soc'] = 1

        self.update_grid_power_rate()

        return self.sim

    def update_grid_power_rate(self):
        # calculate energy flow (grid_power_rate)
        self.sim['energyflow']['grid_power_rate'] = self.sim['energyflow']['consumption_power_rate'] - self.sim['energyflow']['production_power_rate'] - self.sim['energyflow']['battery_power_rate']

    def integrate_power_over_step_interval(self, value):
        # treat seconds as hours
        return value * self.config['simulation_update_interval_sec'] / 3600 * self.config['simulation_speedup']

    def get_behavior(self):
        return "online"

    def get_state(self):
        return self.agent

    def update_state(self, update, timeslot=""):
        # Set action from EMS
        self.agent['energyflow']['grid_import_ems_target'] = update['grid_import_ems_target']
        # Perform step with action on given timeslot + return result
        return self.step(timeslot)