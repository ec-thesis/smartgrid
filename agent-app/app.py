""" SmartGridAgent (stateless) """

__author__ = "Stephan Podlipnig"

import os
from flask import Flask, request
from agents.factory import *
from datetime import datetime, timedelta
import time

import numpy
import matplotlib.pyplot as plt
import matplotlib.dates as md

def load_config(default_config):
    try:
        param_config = json.loads(os.environ.get('AGENT_CONFIG', "{}"))
        return {**default_config, **param_config}
    except:
        raise ValidationError("Invalid or missing config json")


config = load_config({
    "type": "simulation",

    # simulation
    'simulation_update_interval_sec': 1,
    'simulation_speedup': 3600, # use 3600 to treat kwsec as 1 kwhour
    'simulation_excess_strategy': 'prefer_battery',
    'simulation_covering_strategy': 'prefer_battery',
    'simulation_fault_injection': None,
    'simulation_load_shifting': None, #None, SumZeroWithConsShift50perc2h, SumZeroWithConsShift50perc4h, SumZeroWithAllShift50perc2h, SumZeroWithAllShift50perc4h

    # profile
    'profile': '',
    'profile_basefolder': 'profiles/'
})

app = Flask(__name__)
agent = AgentFactory(config['type'], config).getAgent()

# GET STATE for EMS
@app.route('/', methods = ['GET'])
def get_state():
    # Must not be ignored (step required for get_behavior etc.)
    data = agent.step(request.args.get('timeslot', default='', type=str))

    # Simulate behavior
    if agent.get_behavior() == "online":
        return {"data": data}, 200
    else:
        #time.sleep(5)
        return b'504 Gateway Timeout', 504

# PUT ACTION by EMS
@app.route('/', methods = ['PUT'])
def update_state():
    # Simulate behavior
    if agent.get_behavior() == "online":
        # Apply new power target
        return {"data": agent.update_state(request.get_json(), request.args.get('timeslot', default='', type=str))}, 200
    else:
        #time.sleep(5)
        return b'504 Gateway Timeout', 504

# For simulation and tracking purposes only, to complete current timestep, apply energy usage, and return results for tracking only
@app.route('/', methods = ['POST'])
def finalize_state():
    # Perform step with action on given timeslot + return simulated result at timeslot+15min
    return {"data": agent.step(request.args.get('timeslot', default='', type=str), True)}, 200

@app.route('/healthz')
def main():
    return 'OK\r\n', 200


####
pushlist = {}
def pushStep(ts):
    pushlist[ts] = dict(agent.step(ts)['energyflow'])
    print(pushlist[ts])

def getEnergyFlowSeries(key, scalefactor=1):
    series = {}
    for k, v in pushlist.items():
        series[k] = v[key] * scalefactor
    return series

def plotGraph():

    plt.interactive(True)
    plt.figure()
    fig, ax1 = plt.subplots()
    fig.set_size_inches(20, 10)
    plt.gca().xaxis.set_minor_locator(md.HourLocator(interval=6))  # every x hours
    plt.gca().xaxis.set_minor_formatter(md.DateFormatter('%H:%M'))  # hours and minutes
    plt.gca().xaxis.set_major_locator(md.DayLocator(interval=1))  # every day
    plt.gca().xaxis.set_major_formatter(md.DateFormatter('\n%Y-%m-%d'))

    # Y-axis left
    dates = [numpy.datetime64(ts) for ts in getEnergyFlowSeries("consumption_energy_relative").keys()]
    ax1.set_ylabel("kW")
    ax1.plot_date(dates, getEnergyFlowSeries("consumption_power_rate", 0.001).values(), 'b', label='consumption [kW]')
    ax1.plot_date(dates, getEnergyFlowSeries("production_power_rate", 0.001).values(), 'y', label='pv production [kW]')
    ax1.plot_date(dates, getEnergyFlowSeries("grid_power_rate", 0.001).values(), 'gray', label='grid import [kW]')
    ax1.plot_date(dates, getEnergyFlowSeries("battery_power_rate", 0.001).values(), 'r', label='battery discharge [kW]')
    ax1.margins(0, 0)
    plt.legend()

    # Y-axis right
    ax2 = ax1.twinx()
    ax2.set_ylabel('%')
    ax2.set_ylim(ymin=0, ymax=100)
    ax2.plot_date(dates, getEnergyFlowSeries("battery_soc", 100).values(), 'r--,', label='battery SoC [%]')
    ax2.plot_date(dates, getEnergyFlowSeries("acc_selfconsumption_ratio", 100).values(), 'y--', label='Self consumption [%]')
    plt.legend()

    plt.ion()
    #plt.show(block=True)
    plt.savefig(config['profile_basefolder'] + '/' + config['profile'] + '.png', dpi=300, bbox_inches='tight')

if __name__ == "__main__":
    for i in numpy.arange(datetime(2022,8,29,0,0,0), datetime(2022,8,30,23,45,0), timedelta(minutes=15)).astype(datetime):
        pushStep(str(i).replace(" ", "T"))

    plotGraph()
