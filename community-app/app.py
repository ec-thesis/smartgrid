""" SmartGridCommunity (stateless) """

__author__ = "Stephan Podlipnig"

import os
import json
import asyncio
import httpx
from functools import reduce
from datetime import datetime
from flask import Flask, request
from exceptions import *

app = Flask(__name__)

def load_config(default_config):
    try:
        param_config = json.loads(os.environ.get('COMMUNITY_CONFIG', "{}"))
        member_config = {'members': json.loads(os.environ.get('COMMUNITY_MEMBERS', "[]"))}
        return {**default_config, **param_config, **member_config}
    except:
        raise ValidationError("Invalid or missing config json")


config = load_config({
    'timeout_sec': 3,
    'member_url_schema': 'http://smartgridagent-{}-svc:8080/',
    'members': []
})

def request_async(client, url, method='GET', json=None):
    # https://www.python-httpx.org/api/#Client
    return client.request(method, url, timeout=config['timeout_sec'], json=json)

def get_timestamp_floored_to_quarter_hour():
    return (lambda dt: datetime(dt.year, dt.month, dt.day, dt.hour, 15*(dt.minute // 15), ))(datetime.utcnow()).strftime("%Y-%m-%dT%H:%M:%SZ")


def process_response(result, success, failed):
    valid_members = list(filter(lambda p: p['energyflow']['grid_frequency'] > 47.0 and p['energyflow']['grid_frequency'] < 53.0, result.values()))
    valid_producers = list(filter(lambda p: p['is_producer'], valid_members))
    valid_esr = list(filter(lambda p: p['is_esr'], valid_members))
    valid_nonesr = list(filter(lambda p: not p['is_esr'], valid_members))

    valid_grid_importing = list(filter(lambda p: p['energyflow']['grid_power_rate'] >= 0, valid_members))
    valid_grid_exporting = list(filter(lambda p: p['energyflow']['grid_power_rate'] < 0, valid_members))

    valid_grid_importing_esr = list(filter(lambda p: p['energyflow']['grid_power_rate'] >= 0, valid_esr))
    valid_grid_exporting_esr = list(filter(lambda p: p['energyflow']['grid_power_rate'] < 0, valid_esr))
    valid_grid_importing_nonesr = list(filter(lambda p: p['energyflow']['grid_power_rate'] >= 0, valid_nonesr))
    valid_grid_exporting_nonesr = list(filter(lambda p: p['energyflow']['grid_power_rate'] < 0, valid_nonesr))

    data = {
        "timestamp": datetime.now().astimezone().isoformat(),
        "all_members": config['members'],
        "online_members": [key for (key, value) in result.items() if value['is_available']],
        "online_esr": [key for (key, value) in result.items() if value['is_available'] and value['is_esr']],
        "online_producers": [key for (key, value) in result.items() if value['is_available'] and value['is_producer']],
        "failed_members": [key for (key, value) in result.items() if not value['is_available']],
        "energyflow": {
            "total_grid_power_rate": reduce(lambda sum, elm: sum + elm['energyflow']['grid_power_rate'], valid_members, 0),  # negative=overproduction
            "total_grid_power_import_rate": reduce(lambda sum, elm: sum + elm['energyflow']['grid_power_rate'], valid_grid_importing, 0),
            "total_grid_power_export_rate": reduce(lambda sum, elm: sum + abs(elm['energyflow']['grid_power_rate']), valid_grid_exporting, 0),
            "total_grid_power_rate_nonesr": reduce(lambda sum, elm: sum + elm['energyflow']['grid_power_rate'], valid_nonesr, 0),
            "total_grid_power_import_rate_nonesr": reduce(lambda sum, elm: sum + elm['energyflow']['grid_power_rate'], valid_grid_importing_nonesr, 0),
            "total_grid_power_export_rate_nonesr": reduce(lambda sum, elm: sum + abs(elm['energyflow']['grid_power_rate']), valid_grid_exporting_nonesr, 0),
            "total_grid_energy_import_within_timeslot": reduce(lambda sum, elm: sum + elm['energyflow']['grid_energy_imported_relative'], valid_members, 0),
            "total_grid_energy_export_within_timeslot": reduce(lambda sum, elm: sum + elm['energyflow']['grid_energy_exported_relative'], valid_members, 0),
            "total_grid_max_import_power_rate": reduce(lambda sum, elm: sum + elm['energyflow']['grid_max_import_power_rate'], valid_members, 0),
            "total_grid_max_export_power_rate": reduce(lambda sum, elm: sum + elm['energyflow']['grid_max_export_power_rate'], valid_members, 0),
            "total_consumption_power_rate": reduce(lambda sum, elm: sum + elm['energyflow']['consumption_power_rate'], valid_members, 0),
            "total_consumption_energy_within_timeslot": reduce(lambda sum, elm: sum + elm['energyflow']['consumption_energy_relative'], valid_members, 0),
            "avg_per_member_grid_power_rate": reduce(lambda sum, elm: sum + elm['energyflow']['grid_power_rate'], valid_members, 0) / max(1, len(valid_members)),
            "avg_per_member_grid_frequency": reduce(lambda sum, elm: sum + elm['energyflow']['grid_frequency'], valid_members, 0) / max(1, len(valid_members)),

            # producers only
            "total_production_power_rate": reduce(lambda sum, elm: sum + elm['energyflow']['production_power_rate'], valid_producers, 0),
            "total_max_production_power_rate": reduce(lambda sum, elm: sum + elm['energyflow']['max_production_power_rate'], valid_producers, 0),
            "total_production_energy_within_timeslot": reduce(lambda sum, elm: sum + elm['energyflow']['production_energy_relative'], valid_producers, 0),

            # ESRs only
            "total_grid_power_rate_esr": reduce(lambda sum, elm: sum + elm['energyflow']['grid_power_rate'], valid_esr, 0),  # negative=overproduction
            "total_grid_power_import_rate_esr": reduce(lambda sum, elm: sum + elm['energyflow']['grid_power_rate'], valid_grid_importing_esr, 0),
            "total_grid_power_export_rate_esr": reduce(lambda sum, elm: sum + abs(elm['energyflow']['grid_power_rate']), valid_grid_exporting_esr, 0),
            "avg_battery_soc": reduce(lambda sum, elm: sum + elm['energyflow']['battery_soc'], valid_esr, 0) / max(1, len(valid_esr)),
            "total_battery_energy_available": reduce(lambda sum, elm: sum + elm['energyflow']['battery_energy_available'], valid_esr, 0),
            "total_max_battery_energy_controllable": reduce(lambda sum, elm: sum + elm['energyflow']['battery_max_energy_available'], valid_esr, 0),
            "total_battery_max_charging_power_rate": reduce(lambda sum, elm: sum + elm['energyflow']['battery_max_charging_power_rate'], valid_esr, 0),
            "total_battery_max_discharging_power_rate": reduce(lambda sum, elm: sum + elm['energyflow']['battery_max_discharging_power_rate'], valid_esr, 0)

        },
        "details": result
    }

    # enrich
    data['online_member_rate'] = len(data['online_members']) / max(1, len(data['all_members']))
    data['online_producer_rate'] = len(data['online_producers']) / max(1, len(data['online_members']))
    data['online_esr_rate'] = len(data['online_esr']) / max(1, len(data['online_members']))

    return data


async def query_members(current_15min_slot="", method='GET'):
    #if current_15min_slot == "":
    #    current_15min_slot = get_timestamp_floored_to_quarter_hour()
    print('Timeslot: '+current_15min_slot)

    result = {}
    success = []
    failed = []
    async with httpx.AsyncClient(http2=True) as client:
        for member in config['members']:
            try:
                url = ''.join([config['member_url_schema'].format(member), "?timeslot=", current_15min_slot])
                r = await asyncio.ensure_future(request_async(client, url, method))  # method=POST for simulation purposes only
                result[member] = json.loads(r.content)['data']
                success.append(member)
            except BaseException as error:
                print('Failed: ' + member)
                failed.append(member)

    return result, success, failed

async def update_members(actions, current_15min_slot=""):
    #if current_15min_slot == "":
    #    current_15min_slot = get_timestamp_floored_to_quarter_hour()
    print('Timeslot: '+current_15min_slot)

    result = {}
    success = []
    failed = []

    async with httpx.AsyncClient(http2=True) as client:
        for member in config['members']:
            if member in actions:
                action = actions[member]
            else:
                action = {}
            try:
                url = ''.join([config['member_url_schema'].format(member), "?timeslot=", current_15min_slot])
                r = await asyncio.ensure_future(request_async(client, url, 'PUT', action))
                print("RESPONSE")
                print(r.content)
                result[member] = json.loads(r.content)['data']
                success.append(member)
            except BaseException as error:
                print('Failed: ' + member)
                failed.append(member)

    return result, success, failed

# Query all members and output the result
# POST is used for simulation/tracking purposes only
@app.route('/', methods = ['GET', 'POST'])
def get_state():
    res, succ, fail = asyncio.run(query_members(request.args.get('timeslot', default="", type=str), request.method))
    data = process_response(res, succ, fail)
    print(data)
    return {"data": data}, 200

# Propagate actions to all members
@app.route('/', methods = ['PUT'])
def set_state():
    print("PUT request:")
    print(request.get_json())
    res, succ, fail = asyncio.run(update_members(request.get_json(), request.args.get('timeslot', default="", type=str)))
    data = process_response(res, succ, fail)
    print(data)
    return {"data": data}, 200

@app.route('/healthz')
def main():
    return 'OK\r\n', 200


if __name__ == "__main__":
    pass
    #set_state()
